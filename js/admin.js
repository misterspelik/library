jQuery(document).ready(function(){
	var input = document.getElementById("file_upload");
	FileAPI.event.on(input, "change", function (){
		var list = FileAPI.getFiles(input); // Получаем список файлов

		// Загружаем на сервер
		FileAPI.upload({
			 url: "/upload/upload_image_ajax/",
			 files: { userFiles: list },
			 filecomplete: function (err, xhr){
				var response= xhr.responseText;

				switch(response){
					case 'couldnt_move_file':
						jQuery("#result_files").html("Не удалось переместить файл из временноой папки.");
					break;
					case 'couldnt_create_dir':
						jQuery("#result_files").html("Целевая папка для картинок (<strong>/uploads/images/</strong>) отсутствует. И не удалось её создать.");
					break;
					default:
						var img= '<img src="'+response+'" height="100"/>';
						jQuery("#result_files").html(img);
						jQuery("#thumb").val(response);
					break;
				}
				jQuery("#result_files").show();
			 }
		});
	});

});
