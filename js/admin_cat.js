function form_parent_url(){
	var name= $('input[name="name"]').val();
	var parent= $('select[name="parent_id"]').val();
	
	$.ajax({
		type: "POST",
		url: "/admin/form_page_url",
		data: {'name': name},
		success: function(response){
			$('input[name="url"]').val(response);
		}
	});
}

jQuery(document).ready(function(){
	form_parent_url();
	$('input[name="name"]').keyup(function(){
		 form_parent_url();
	});
	

});