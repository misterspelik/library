<? $this->load->view('admin/head', array('title'=>$title)); ?>
<div class="admin_content" style="width:1000px;margin:auto;padding-top:20px;">
    <a href="/admin/categories" class="btn btn-primary">Вернуться к жанрам</a>
    <p style="height:20px;"></p>
    <?=validation_errors( '<div class="error">', '</div>');?>
        <form class="form-horizontal" role="form" method="post" action="">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Название жанра</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Введите название" value="<?=(!empty($category['name']) ? $category['name'] : '')?>" name="name" autocomplete="off" />
                </div>
            </div>
            <div class="form-group">
                <label for="url" class="col-sm-2 control-label">Url жанра</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="url" placeholder="" value="<?=(!empty($category['url']) ? $category['url'] : '')?>" name="url" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">
                        <?=(!empty($category) ? 'Редактировать' : 'Добавить ')?>жанр</button>
                </div>
            </div>
        </form>
</div>
<? $this->load->view('admin/foot'); ?>


<script type="text/javascript">
    
    $(document).ready(function() {
        form_parent_url();
        $('input[name="name"]').keyup(function() {
            form_parent_url();
        });
    });

    function form_parent_url() {
        var name = $('input[name="name"]').val();

        $.ajax({
            type: "POST",
            url: "/admin/form_page_url",
            data: {
                'name': name
            },
            success: function(response) {
                $('input[name="url"]').val(response);
            }
        });
    }
</script>