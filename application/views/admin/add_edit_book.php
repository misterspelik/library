<? $this->load->view('admin/head', array('title'=>$title)); ?>
<div class="admin_content" style="width:1000px;margin:auto;padding-top:20px;">
	<a href="/admin/books" class="btn btn-primary">Вернуться к книгам</a>
	<p style="height:20px;"></p>
	<?=validation_errors('<div class="error">', '</div>');?>
	<form class="form-horizontal" role="form" method="post" action="">
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Название книги:</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" placeholder="Введите имя" value="<?=(!empty($book['name']) ? $book['name'] : '')?>" name="name" autocomplete="off"/>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Аннотация:</label>
			<div class="col-sm-10">
				<textarea  name="summary" class="form-control" placeholder="Напишите аннотацию" rows="4"><?=(!empty($book['summary']) ? $book['summary'] : '')?></textarea>
				
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Авторы:</label>
			<div class="col-sm-10">
				<select name="authors[]" class="chosen" multiple style="width:600px;">
					<? foreach($authors as $a){ ?>
						<option value="<?=$a['id']?>" <?=((!empty($book_authors) && in_array($a['id'], $book_authors)) ? 'selected' : '')?>><?=$a['name']?></option>
					<? } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Жанры:</label>
			<div class="col-sm-10">
				<select name="categories[]" class="chosen" multiple style="width:600px;">
					<? foreach($categories as $c){ ?>
						<option value="<?=$c['id']?>" <?=((!empty($book_cats) && in_array($c['id'], $book_cats)) ? 'selected' : '')?>><?=$c['name']?></option>
					<? } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Картинка обложки:</label>
			<div class="col-sm-10">
				<input type="file" class="form-control" placeholder="Выберите файл" value="" name="file_upload" id="file_upload" autocomplete="off"/>
				<input type="hidden" value="<?=(!empty($book['thumb']) ? $book['thumb'] : '')?>" name="thumb" id="thumb"/>
				<div id="result_files" style="margin-top:20px; display:<?=(!empty($book['thumb']) ? 'block' : 'none')?>;">
					<? if (!empty($book['thumb'])){ ?>
						<img src="<?=$book['thumb']?>" height="100"/>
					<? } ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><?=(!empty($book) ? 'Редактировать' : 'Добавить')?> книгу</button>
			</div>
		</div>
	</form>
</div>
<script>
	jQuery('.chosen').chosen();
</script>
<? $this->load->view('admin/foot'); ?>
