<? $this->load->view('admin/head', array('title'=>$title)); ?>
<div class="admin_content" style="width:1000px;margin:auto;padding-top:20px;">
	<a href="/admin/add_category" class="btn btn-primary">Добавить жанр</a>
	<? if (!empty($categories)){ ?>
		<p style="height:20px;"></p>
		<table class="table">
			<tr>
			<td>Номер</td>
			<td>Название</td>
			<td>Просмотр/Правка</td>
			</tr>
			<? foreach($categories as $cat){ ?>
				<tr>
				<td><?=$cat['id']?></td>
				<td><?=$cat['name'];?></td>
				<td><a href="/admin/edit_category/<?=$cat['id']?>" target="_blank">Edit</a></td>
				</tr>
			<? } ?>
		</table>
	<? } ?>
</div>
<? $this->load->view('admin/foot'); ?>
