<? $this->load->view('admin/head', array('not_menu'=>true)); ?>
<div id="login" style=" width:500px; margin:auto;">
	<h2>Вход в админ-панель</h2>
	<?php
		if (isset($err))
			echo $err;
		if (isset($string))
			echo $string; 
	?>
<form class="form-horizontal" role="form" method="post" action="/admin/login">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Логин</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputEmail3" placeholder="user" name="login"/>
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Пароль</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="inputPassword3" placeholder="passphrase" name="password"/>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox"> Remember me
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">Sign in</button>
    </div>
  </div>
</form>


</div>
<? $this->load->view('admin/foot'); ?>
