<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset='utf-8'">
	<title><?=(!empty($title) ? $title : '')?></title>
	<script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>

	<script src="/js/fileAPI/FileAPI.js" type="text/javascript"></script>
	<script src="/js/fileAPIext/canvas-to-blob.js" type="text/javascript"></script>
	<script src="/js/fileAPIext/FileAPI.core.js" type="text/javascript"></script>
	<script src="/js/fileAPIext/FileAPI.XHR.js" type="text/javascript"></script>
	<script src="/js/fileAPIext/FileAPI.Form.js" type="text/javascript"></script>
	<script src="/js/fileAPIext/FileAPI.Image.js" type="text/javascript"></script>
	<script src="/js/fileAPIext/FileAPI.Flash.js" type="text/javascript"></script>
	<script src="/js/fileAPI/FileAPI.exif.js" type="text/javascript"></script>
	<script src="/js/fileAPI/FileAPI.id3.js" type="text/javascript"></script>

	<script type="text/javascript" src="/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="/js/admin.js"></script>

	<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/chosen/chosen.css">
<style>
.admin_menu a{
	margin: 10px;5
}
.active_menu_link{
	text-decoration: underline;
}
</style>
</head>
<body>
<? if (!isset($not_menu)){ ?>
	<h1>Админка библиотеки</h1>
	<div class="admin_menu">
		<a href="/admin/books" class="<?=get_active('/admin/books');?>menu_link">Книги</a>
		<a href="/admin/authors" class="<?=get_active('/admin/authors');?>menu_link">Авторы</a>
		<a href="/admin/categories" class="<?=get_active('/admin/categories');?>menu_link">Жанры</a>
		<a href="/admin/clients" class="<?=get_active('/admin/clients');?>menu_link">Читатели</a>
		<a href="/admin/images" class="<?=get_active('/admin/images');?>menu_link">Изображения</a>
		<a href="/admin/users" class="<?=get_active('/admin/users');?>menu_link">Администраторы</a>
		
		<a href="/admin/logout">Выход</a>
	</div>

	<div class="sidebar">
		<a href=""></a>
		<a href=""></a>

		<a href="">
			<div class="bars">
				
					<div class="bar"></div>
					<div class="bar"></div>
					<div class="bar"></div>
					
					<div class="clear"></div>
				
				</div>
				
				<!--  -->
		</a>
	</div>
<? } ?>
