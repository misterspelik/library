<? $this->load->view('admin/head', array('title'=>$title)); ?>
<div class="admin_content" style="width:1000px;margin:auto;padding-top:20px;">
	<a href="/admin/add_book" class="btn btn-primary">Добавить книгу</a>
	<? if (!empty($books)){ ?>
		<p style="height:20px;"></p>
		<table class="table">
			<tr>
			<td>Номер</td>
			<td>Название</td>
			<td>Рейтинг</td>
			<td>Авторы</td>
			<td>Жанры</td>
			<td>Просмотр/Правка</td>
			</tr>
			<? foreach($books as $id=>$book){ ?>
				<tr>
				<td><?=$id?></td>
				<td><?=$book['name'];?></td>
				<td><?=$book['rating']?></td>
				<td>
					<? if (empty($authors[$id])){ continue; }
						$str= array();
						foreach ($authors[$id] as $a){
							$str[]= '<a href="/admin/edit_author/'.$a['id'].'">'.$a['name'].'</a>';
						}
						echo implode(', ', $str);
					?>
				</td>
				<td>
					<?  $str= array();
						if (!empty($categories[$id])){
							foreach ($categories[$id] as $c){
								$str[]= '<a href="/admin/edit_category/'.$c['id'].'">'.$c['name'].'</a>';
							}
						}
						echo implode(', ', $str);
					?>
				</td>
				<td><a href="/admin/edit_book/<?=$book['id']?>" target="_blank">Edit</a></td>
				</tr>
			<? } ?>
		</table>
	<? } ?>
</div>
<? $this->load->view('admin/foot'); ?>
