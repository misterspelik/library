<? $this->load->view('admin/head', array('title'=>$title)); ?>
<div class="admin_content" style="width:1000px;margin:auto;padding-top:20px;">
	<a href="/admin/authors" class="btn btn-primary">Вернуться к авторам</a>
	<p style="height:20px;"></p>
	<?=validation_errors('<div class="error">', '</div>');?>
	<form class="form-horizontal" role="form" method="post" action="">
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">ФИО автора:</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" placeholder="Введите имя" value="<?=(!empty($author['name']) ? $author['name'] : '')?>" name="name" autocomplete="off"/>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Биография:</label>
			<div class="col-sm-10">
				<textarea  name="description" class="form-control" placeholder="Напишите биографию" rows="4"><?=(!empty($author['description']) ? $author['description'] : '')?></textarea>
				
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><?=(!empty($author) ? 'Редактировать' : 'Добавить')?> автора</button>
			</div>
		</div>
	</form>
</div>
<? $this->load->view('admin/foot'); ?>
