<? $this->load->view('admin/head', array('title'=>$title)); ?>
<div class="admin_content" style="width:1000px;margin:auto;padding-top:20px;">
	<a href="/admin/add_author" class="btn btn-primary">Добавить автора</a>
	<? if (!empty($authors)){ ?>
		<p style="height:20px;"></p>
		<table class="table">
			<tr>
			<td>Номер</td>
			<td>Имя</td>
			<td>Рейтинг</td>
			<td>Об авторе</td>
			<td>Просмотр/Правка</td>
			</tr>
			<? foreach($authors as $author){ ?>
				<tr>
				<td><?=$author['id']?></td>
				<td><?=$author['name'];?></td>
				<td><?=$author['rating']?></td>
				<td><?=(!empty($author['description']) ? mb_substr($author['description'], 0, 30, 'utf-8').' ...' : '')?></td>
				<td><a href="/admin/edit_author/<?=$author['id']?>" target="_blank">Edit</a></td>
				</tr>
			<? } ?>
		</table>
	<? } ?>
</div>
<? $this->load->view('admin/foot'); ?>
