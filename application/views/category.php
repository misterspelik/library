<div class="wrapper" id="wrapper">
	<div class="content">																			                    
		<div class="posts" id="posts">	
		<?php foreach ($books as $book) {?>						
			<div class="post-container">				
					<div class="post type-post status-publish format-image has-post-thumbnail hentry category-design category-furniture category-interior-design tag-alfred post_format-post-format-image">
						<div class="featured-media">	
							<img width="508" height="678" src="<?=$book['thumb']?>" class="attachment-post-thumb wp-post-image" alt="" />				
							<a class="post-overlay" href="/main/get_book/<?=$book['id'];?>" rel="bookmark" title="Alfred Magazine Rack">
								<p class="view">Посмотреть &rarr;</p>
							</a>
						</div> <!-- /featured-media -->
						<div class="post-header">
						    <h2 class="post-title">
						    	<a href="" title=""><?=$book['name'];?></a>
						    </h2>
				    	</div>
				    </div>
			</div>   
		<? } ?> 	
		</div> 
	</div> 
</div> 
