<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ошибочка 503</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0-wip/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">

    <!-- Custom styles for this page -->
    <link rel="stylesheet" href="/css/503.css">
  </head>

  <body>

    <div class="container">
      <!-- Jumbotron -->
      <div class="jumbotron">
        <h1><i class="icon-warning-sign orange"></i> 503 Сервис недоступен</h1>
        <p class="lead">Неожиданно всё упало или даже не работало ;)</p>
        <a href="javascript:document.location.reload(true);" class="btn btn-default btn-lg text-center">
			<span class="green">Попробуйте перезагрузить (но сильно не надейтесь)</span></a>
      </div>
    </div>
    <div class="container">
      <div class="body-content">

        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-6">
            <h2>Что же произошло?</h2>
            <p class="lead">A 503 error status implies that this is a temporary condition due to a temporary overloading or maintenance of the server. This error is normally a brief temporary interuption.</p>
          </div>
          <div class="col-md-6">
            <h2>И что я должен делать?</h2>
            <p class="lead">Если вы посетитель и пришли сюда не просто так</p>
            <p>If you need immediate assistance, please send us an email instead. We apologize for any inconvenience.</p>
            <p class="lead">Если вы - это я (владелец сайта) </p>
             <p>This error is mostly likely very brief, the best thing to do is to check back in a few minutes and everything will probably be working normal agian.</p>
         </div>
        </div>

      </div><!-- /.body-content -->
    </div><!-- /.body-container -->

      <!-- Site footer -->
      <div class="footer">
        <div class="row">
          <div class="col-md-12 jumbotron">
          </div>
        </div>
      </div>
      <!-- /container -->

  </body>
</html>
