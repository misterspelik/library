<!DOCTYPE html>
<html class="no-js" lang="en-US">
	<head profile="http://gmpg.org/xfn/11">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
		<title>Library</title>
		<script>document.documentElement.className = document.documentElement.className.replace("no-js","js");</script>
		<link rel="alternate" type="application/rss+xml" title="Fukasawa &raquo; Feed" href="http://www.andersnoren./themes/fukasawa/feed/" />
		<link rel="alternate" type="application/rss+xml" title="Fukasawa &raquo; Comments Feed" href="http://www.andersnoren.se/themes/fukasawa/comments/feed/" />
		<script type="text/javascript">window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/www.andersnoren.se\/themes\/fukasawa\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.2"}};
		!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
			img.wp-smiley,
			img.emoji {
				display: inline !important;
				border: none !important;
				box-shadow: none !important;
				height: 1em !important;
				width: 1em !important;
				margin: 0 .07em !important;
				vertical-align: -0.1em !important;
				background: none !important;
				padding: 0 !important;
			}
		</style>
		<link rel='stylesheet' id='mediaelement-css'  href='http://www.andersnoren.se/themes/fukasawa/wp-includes/js/mediaelement/mediaelementplayer.min.css?ver=2.16.2' type='text/css' media='all' />
		<link rel='stylesheet' id='wp-mediaelement-css'  href='http://www.andersnoren.se/themes/fukasawa/wp-includes/js/mediaelement/wp-mediaelement.css?ver=4.2.2' type='text/css' media='all' />
		<link rel='stylesheet' id='jetpack_css-css'  href='http://www.andersnoren.se/themes/fukasawa/wp-content/plugins/jetpack/css/jetpack.css?ver=3.6' type='text/css' media='all' />
		<link rel='stylesheet' id='fukasawa_googleFonts-css'  href='//fonts.googleapis.com/css?family=Lato%3A400%2C400italic%2C700%2C700italic&#038;ver=4.2.2' type='text/css' media='all' />
		<link rel='stylesheet' id='fukasawa_genericons-css'  href='http://www.andersnoren.se/themes/fukasawa/wp-content/themes/fukasawa/genericons/genericons.css?ver=4.2.2' type='text/css' media='all' />
		<link rel='stylesheet' id='fukasawa_style-css'  href='http://www.andersnoren.se/themes/fukasawa/wp-content/themes/fukasawa/style.css?ver=4.2.2' type='text/css' media='all' />
		<script type='text/javascript' src='http://www.andersnoren.se/themes/fukasawa/wp-includes/js/jquery/jquery.js?ver=1.11.2'></script>
		<script type='text/javascript' src='http://www.andersnoren.se/themes/fukasawa/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
		<script type='text/javascript' src='http://www.andersnoren.se/themes/fukasawa/wp-content/plugins/jetpack/_inc/spin.js?ver=1.3'></script>
		<script type='text/javascript' src='http://www.andersnoren.se/themes/fukasawa/wp-content/plugins/jetpack/_inc/jquery.spin.js?ver=1.3'></script>
		<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.andersnoren.se/themes/fukasawa/xmlrpc.php?rsd" />
		<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.andersnoren.se/themes/fukasawa/wp-includes/wlwmanifest.xml" /> 
		<meta name="generator" content="WordPress 4.2.2" />
		 
	</head>
	
