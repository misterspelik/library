<?php foreach ($one_book as $book) {?>
	<div class="wrapper" id="wrapper">
		<div class="content thin">									        
			<div id="post-125" class="single post-125 post type-post status-publish format-image has-post-thumbnail hentry category-design category-furniture category-interior-design tag-alfred post_format-post-format-image">
				<div class="featured-media">
					<img width="973" height="1300" src="<?=$book['thumb']?>" class="attachment-post-image wp-post-image" alt="<?=$book['name']?>" />					
				</div> <!-- /featured-media -->
				
				<div class="post-inner">
					<div class="post-header">
						<h2 class="post-title"><?=$book['name']?></h2>
					</div> <!-- /post-header -->
				    <div class="post-content">
			        	<p><?=$book['summary']?></p>
			        </div> <!-- /post-content -->
			        <div class="clear"></div>
					
				</div> 
				<div class="respond-container">
					<div id="respond" class="comment-respond">
						<h3 id="reply-title" class="comment-reply-title">Оставить комментарий</h3>
						<div id="disqus_thread"></div>
						<script type="text/javascript">
					    /* * * CONFIGURATION VARIABLES * * */
					    	var disqus_shortname = 'libraryofbooks';
					    
					    /* * * DON'T EDIT BELOW THIS LINE * * */
					    	(function() {
						        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
						        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
						        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
					    	})();
						</script>
						<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
					</div><!-- #respond -->
				</div> <!-- /respond-container -->		
			</div>
		</div> <!-- /content thin -->
	</div> 
<?} ?>
