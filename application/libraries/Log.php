<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Log{

		function __construct($params= array()){
			$this->file= $_SERVER['DOCUMENT_ROOT'].(empty($params['name']) ? '/logs/admin_log' : '/logs/'.$params['name']);
		}
				
		function write($name='admin', $action=''){
			$what= "User: ".$name." | Date: ".date("d.m.Y H:i")." | Action: ".$action."\n";
			file_put_contents($this->file, $what, FILE_APPEND);
		}
		
		function write_api($params= array()){
			$what= "User: ".$params['user']." | Date: ".date("d.m.Y H:i")." | Method: ".$params['method']." | IP: ".$params['ip']."\n";
			file_put_contents($this->file, $what, FILE_APPEND);
		}
	}

?>
