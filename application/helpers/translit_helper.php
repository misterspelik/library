<?php

if (!function_exists('_tr')) {

    /**
     * @параметр-строка $text - текст для транслитерации.
     * @параметр-строка $rule - правила транслитерации (google или iso-9, или gost).
     * @параметр-строка $word_spliter - спецсимвол для разделения слов.
     * @возвращаемая строка - транслитерированный текст.
     */
    function _tr($text, $rule = 'google', $word_spliter = '-') {
        $text = mb_strtolower($text, 'UTF-8');
        /* FIX looping */
        if (preg_match('#^([a-z0-9]+)$#i', $word_spliter) > 0) {
            $word_spliter = '-';
        }
        /* правила транслитерации */
        $tr_array = array();
        switch ($rule) {

            case 'iso-9':
                $tr_array = array(
                    "а" => "a", "ци" => "ci", "цe" => "ce",
                    "б" => "b", "в" => "v", "г" => "g", "д" => "d",
                    "е" => "e", "ё" => "yo", "ж" => "zh", "з" => "z",
                    "и" => "i", "й" => "j", "к" => "k", "л" => "l",
                    "м" => "m", "н" => "n", "о" => "o", "п" => "p",
                    "р" => "r", "с" => "s", "т" => "t", "у" => "u",
                    "ф" => "f", "х" => "kh", "ц" => "cz", "ч" => "ch",
                    "ш" => "sh", "щ" => "shh", "ь" => "", "ы" => "y",
                    "ъ" => "", "э" => "e", "ю" => "yu", "я" => "ya",
                    "йо" => "yo", "ї" => "yi", "і" => "i", "є" => "ye",
                    "ґ" => "g"
                );
                break;
            case 'gost':
                $tr_array = array(
                    "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
                    "е" => "e", "ё" => "jo", "ж" => "zh",
                    "з" => "z", "и" => "i", "й" => "jj", "к" => "k", "л" => "l",
                    "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
                    "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "kh",
                    "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "shh", "ъ" => "",
                    "ы" => "y", "ь" => "", "э" => "eh", "ю" => "yu", "я" => "ya"
                );
                break;

            default:
            case 'google':
                $tr_array = array(
                    "а" => "a", "ый" => "iy", "ые" => "ie",
                    "б" => "b", "в" => "v", "г" => "g", "д" => "d",
                    "е" => "e", "ё" => "yo", "ж" => "zh", "з" => "z",
                    "и" => "i", "й" => "y", "к" => "k", "л" => "l",
                    "м" => "m", "н" => "n", "о" => "o", "п" => "p",
                    "р" => "r", "с" => "s", "т" => "t", "у" => "u",
                    "ф" => "f", "х" => "kh", "ц" => "ts", "ч" => "ch",
                    "ш" => "sh", "щ" => "shch", "ь" => "", "ы" => "y",
                    "ъ" => "", "э" => "e", "ю" => "yu", "я" => "ya",
                    "йо" => "yo", "ї" => "yi", "і" => "i", "є" => "ye",
                    "ґ" => "g"
                );
                break;
        }
        /* замена кириллических символов */
        $out = str_ireplace(array_keys($tr_array), array_values($tr_array), $text);

        /* нормализация теккста для url - в нижний регистр */
        $out = mb_strtolower($out, 'UTF-8');

        $out = preg_replace('#([^a-z0-9]+)#i', $word_spliter, $out);
        $double_spliter = $word_spliter . $word_spliter;
        $count = 1;
        while ($count > 0) {
            $out = str_replace($double_spliter, $word_spliter, $out, $count);
        }

        $out = trim($out, $word_spliter);
        return $out;
    }
}
?>