<?php

class Migration_add_url_in_categories extends CI_Migration {

	public function up()
	{
		
		$this->db->query("ALTER TABLE `categories` ADD `url` varchar(255) NOT NULL ;");
	
	}

	public function down()
	{
		$this->db->query("ALTER TABLE `categories` DROP `url`;");
		
	}
}