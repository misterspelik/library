<?php

class Main extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	function index(){
		$this->load->view('503');
	}

	/**
	 * 	Показ списка книг
	 * 	
	 * 	@return void
	**/

	public function show_books(){
		$this->load->view('parts/header');
		$data['categories'] = $this->admin_model->get_categories();
		$data['books'] = $this->admin_model->get_books();
		$this->load->view('parts/sidebar', $data);
		$this->load->view('books', $data);
		$this->load->view('parts/footer');
	}

	/**
	 * 	Страница конкретной книги
	 * 	@param int $id			айди книги
	 * 	@return void
	**/
	public function get_book($id){
		$this->load->view('parts/header');
		$data['categories'] = $this->admin_model->get_categories();
		$data['books'] = $this->admin_model->get_books();
		$data['one_book'] = $this->admin_model->get_book_by_id($id);
		$this->load->view('parts/sidebar', $data);
		$this->load->view('one_book', $data);
		$this->load->view('parts/footer');
	}

	/**
	 * 	Показ списка книг конктреного жанра
	 * 	
	 * 	@return void
	**/
	public function show_category($name){
        $this->load->view('parts/header');
        $category_id = $this->admin_model->get_category_by_url($name);
        $data['books'] = $this->admin_model->get_books_by_category($category_id['0']['id']);
        $data['categories'] = $this->admin_model->get_categories();
        $this->load->view('parts/sidebar', $data);
        $this->load->view('category', $data);
        $this->load->view('parts/footer');
     }
}
