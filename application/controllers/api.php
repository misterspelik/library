<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 	Контроллер для работы API
 * 
**/
class Api extends CI_Controller{
	public $user= array();
	
	function __construct(){
		parent::__construct();
		$this->load->model('api_model');
		$this->load->model('admin_model');
		$this->load->model('settings_model');
		
		$this->settings= $this->settings_model->getAllSettings();
	}

	/**
	 * 	Точка входа в API
	**/
	function index(){
		header('Content-Type: application/json');

		$post= $this->input->post(NULL, TRUE);
		$auth= $this->api_model->authorize($post);

		if( !isset($auth['status']) || $auth['status'] !== 'OK'){
			$error_message = (isset($auth['message'])) ? $auth['message'] : 'An error occurred';
			$this->show_error('AuthError', $error_message);
		}else{
			$this->user['privileges']= $auth['privileges'];
		}
		$method= $post['method'];
		$params= !empty($post['params']) ? $post['params'] : array();

		if (!method_exists($this, $method)){
			$this->show_error('GeneralError', 'Unknown API method: '.$method);
		}else{
			/* есть ли привилегия на использование метода */
			$can_use= $this->_check_privileges($method);
			if ($can_use){
				$this->$method($params);
			}else{
				$this->show_error('GeneralError', 'You can`t use method: '.$method.'. Call Admin.');
			}
		}
	}

	/**
	 * 	Проверка привилегии пользователя на метод API
	 * 	@param str $method				имя метода
	 * 	@return bool
	**/
	private function _check_privileges($method){
		if (empty($this->user['privileges'])){
			return FALSE;
		}
		if ($this->user['privileges'][0] == 'all' || in_array($method, $this->user['privileges'])){
			return TRUE;
		}
		return FALSE;
	}
	
	/** 
	 *	Проверка работы API
	**/
	private function ping($params) {
		$this->respond('OK');
	}

	/**
	 * 	Возвращение ошибки от API
	 * 	@param str $type		тип ошибки
	 * 	@param str $message		данные об ошибке
	 * 	@return(echo) json
	**/
	private function show_error($type, $message) {
		$response = array(
			'error' => array(
				'type'		=> $type,
				'message'	=> $message
			),
			'success'	=> false
		);
		echo json_encode($response);
		exit;
	}
	
	/**
	 * 	Возвращение успешного ответа от API
	 * 	@param $data		результат работы метода
	 * 	@return(echo) json
	**/
	private function respond($data) {
		$response = array(
			'success'	=> true,
			'data'	=> $data,
		);
		$from= array('{', '}');
		$to= array('[', ']');
		echo str_replace($from, $to, json_encode($response));
		exit;
	}

	/**
	 * 	Получение топ 5 книг
	 * 	@return void
	**/
	private function get_top_books($params= array()){
		//есть ли ограничение на выборку
		if (!isset($params['limit'])){
			$limit= 5;
		}else{
			$limit= $params['limit'];
		}
		//есть ли смещение выборки
		if (!isset($params['offset'])){
			$offset= 0;
		}else{
			$offset= $params['offset'];
		}

		$result= $this->admin_model->get_books($limit, $offset, array('order'=>'rating', 'order_way'=>'desc', 'fields'=>'id, name, thumb'));

		$this->respond($result);
	}
	
	/**
	 * 	Получение книги по её айди
	 * 	@param int $params[id]		айди книги
	 * 	@return void
	**/
	private function get_book($params= array()){
		if (empty($params['id'])){
			$this->show_error('MethodError', 'There isn`t book ID in params.');
		}
		$result= array(
			'book' => $this->admin_model->get_books(0, 0, array('ids'=>array($params['id']))),
			'categories' => $this->admin_model->get_categories(array($params['id'])),
			'authors' => $this->admin_model->get_authors(array($params['id']))
		);

		$this->respond($result);
	}

}
