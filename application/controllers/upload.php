<?php
/**
 * 	Контроллер загрузчика файлов
 * 
**/
class Upload extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}

	/**
	 * 	Загрузка файлов через JS
	 * 	@param $_FILES
	 * 	@return void
	**/
	public function upload_image_ajax(){
		if (!$this->input->is_ajax_request()){
		   exit('no_direct_access');
		}
		$folder= '/uploads/images/';
		$path= $_SERVER['DOCUMENT_ROOT'].$folder;
		$file= $_FILES['userFiles'];
		
		$this->_manage_folder($folder);

		if ( !move_uploaded_file($file['tmp_name'], $path.$file['name']) ){
			exit('couldnt_move_file');
		}else{
			chmod($path.$file['name'], 0755);
			echo $folder.$file['name'];
		}
	}

	private function _manage_folder($folder){
		$path= $_SERVER['DOCUMENT_ROOT'].$folder;
		if (!is_dir($path)){
			$md= mkdir($path, 0777, true);
			if (!$md){
				exit('couldnt_create_dir');
			}
		}
	}
}
