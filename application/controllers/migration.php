<?php

class Migration extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library('migration');
	}
	
	private function _show_error(){
		show_error($this->migration->error_string());
		die();
	}
	
	/**
	 *	Текущая миграция базы данных
	 *	(установлен в $config['migration_version'])
	 *
	 *	return void
	**/
	public function migration_current(){
		if (!$this->migration->current()){
			$this->_show_error();
		}
		
		echo 'Migrated successfully.';
	}
	
	/**
	 *	Миграция базы данных на самую новую версию
	 *	(найденную в ФС)
	 *
	 *	return void
	**/
	public function migration_lastest(){
		if (!$this->migration->latest()){
			$this->_show_error();
		}
		
		echo 'Migrated successfully.';
	}
	
	/**
	 *	Миграция базы данных на указанную версию
	 *	(номер в настройках игнорируется)
	 *	@param int $version				версия миграции
	 *
	 *	return void
	**/
	public function migration_version($version){
		if (!$this->migration->version($version)){
			$this->_show_error();
		}
		
		echo 'Migrated successfully.';
	}
}