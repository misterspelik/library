<?php
/**
 * 	Контроллер админки
 * 
**/
class Admin extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('settings_model');
		$this->load->model('admin_model');
		$this->load->helper('url');
		$this->load->helper('admin');
		$this->load->library('log');
		
		//если пользователь не залогинен, то показываем ему форму логина
		if (!isset($this->session->userdata['logged_in']) && strpos($this->config->item('uri'), 'admin/login')==false){
			redirect('/admin/login');
		}

		$this->title.= 'Админка библиотеки';			
		$this->settings= $this->settings_model->getAllSettings();
	}
	
	/**
	 * 	Начальная страница при входе в админку
	 * 	@return void
	**/
	public function index(){
		$data['title']= $this->title;
		
		$this->load->view('admin/index', $data);
	}

	/**
	 * 	Логин в админке
	 * 	@param $_POST
	 * 	@return void
	**/
	public function login(){
		$this->load->library('form_validation');
		//проверка полей формы на адекватность
		$config = array(
		   array(
				 'field'   => 'login',
				 'label'   => '"Логин"',
				 'rules'   => 'trim|required|min_length[3]'
			  ),
		   array(
				 'field'   => 'password',
				 'label'   => '"Пароль"',
				 'rules'   => 'required'
			  )
		);
		$this->form_validation->set_rules($config);
					
		if ($this->form_validation->run()==true) { //если проверка прошла
			$this->load->model('users_model');

			$post= $this->input->post(NULL, TRUE);

			//проверка на подходящие логины/пароли для администраторов
			$pass= $this->users_model->encrypt_password($post['password'], $post['login']);
			$data= $this->db
				->where(array('username'=>$post['login'], 'password'=>$pass))
				->get('admin')->row_array();
			
			if (!empty($data)){
				$admindata = array(
					'id'		=> $data['id'],
					'username'  => $data['username'],
					'email'     => $data['email'],
					'logged_in' => true
				);
				$this->session->set_userdata($admindata);
				
				$this->log->write($this->session->userdata['username'], "Login in admin");
				
				redirect('admin');
			}else{
				$Err['string']="Неправильное имя пользователя или пароль!!";
				$this->load->view('admin/login', $Err);
			}
		}
		else{ //если проверка формы не прошла
			$data['err']=$this->form_validation->error_string();
			$data['string']="";	
			$this->load->view('admin/login', $data);
			return;
		}
	}
	
	/**
	 * 	Выход из админки
	 * 	@return void
	**/
	public function logout(){
		$this->log->write($this->session->userdata['username'], "Logout from admin");
		$this->session->sess_destroy();
		header("Location: http://".$_SERVER['HTTP_HOST'].'/admin');
		exit();
	}
	
	/**
	 * 	Страница "Книги"
	 * 	@return void
	**/
	public function books(){
		$data['title']= 'Книги | '.$this->title;
		$data['books']= $this->admin_model->get_books();
		$ids= array_keys($data['books']);
		$data['authors']= $this->admin_model->get_authors($ids);
		$data['categories']= $this->admin_model->get_categories($ids);

		$this->load->view('admin/books', $data);
	}
	
	/**
	 * 	Страница "Авторы"
	 * 	@return void
	**/
	public function authors(){
		$data['title']= 'Авторы | '.$this->title;
		$data['authors']= $this->admin_model->get_all_authors();
		
		$this->load->view('admin/authors', $data);
	}
	
	/**
	 * 	Страница "Жанры"
	 * 	@return void
	**/
	public function categories(){
		$data['title']= 'Жанры | '.$this->title;
		$data['categories']= $this->admin_model->get_categories();
		
		$this->load->view('admin/categories', $data);
	}

	/**
	 * 	Страница "Изображения"
	 * 	@return void
	**/
	public function images(){
		$data['title']= 'Изображения | '.$this->title;
	}
	
	public function clients(){
		$data['title']= 'Читатели | '.$this->title;
		$data['clients']= $this->admin_model->get_all_clients();

		$this->load->view('admin/clients', $data);
	}

	/**
	 * 	Страница добавления книги
	 * 	@return void
	**/
	public function add_book(){
		$this->load->library('form_validation');
		//проверка полей формы на адекватность
		$rules = array(
			array(
				'field'   => 'name',
				'label'   => '"Название книги"',
				'rules'   => 'trim|required|min_length[3]'
			),
			array(
				'field'   => 'summary',
				'label'   => '"Аннотация"',
				'rules'   => 'trim|required|min_length[10]'
			),
		);
		$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() === TRUE){
			$post= $this->input->post(NULL, TRUE);
			$insert= array(
				'name' => $post['name'],
				'rating' => 0,
				'created'=> time(),
				'creator_id' => $this->session->userdata['id'] ? $this->session->userdata['id'] : 0,
				'thumb' => $post['thumb'],
				'summary' => $post['summary'],
			);
			$this->db->insert('books', $insert);
			$id= $this->db->insert_id();
			if (!empty($post['authors'])){
				$a_ins= array();
				foreach ($post['authors'] as $pa){
					$a_ins[]= array('book_id'=>$id, 'author_id'=>$pa);
				}
				$this->db->insert_batch('books_authors', $a_ins);
			}
			if (!empty($post['categories'])){
				$c_ins= array();
				foreach ($post['categories'] as $pc){
					$c_ins[]= array('book_id'=>$id, 'category_id'=>$pc);
				}
				$this->db->insert_batch('books_categories', $c_ins);
			}
			redirect('/admin/edit_book/'.$id);
		}
		else{
			$data= array(
				'title'		=> 'Добавить книгу | '.$this->title,
				'authors'	=> $this->admin_model->get_all_authors(array('fields'=>'id, name')),
				'categories' => $this->admin_model->get_categories()
			);

			$this->load->view('admin/add_edit_book', $data);
		}
	}
	
	/**
	 * 	Страница редактирования книги
	 * 	@param int $id			айди книги
	 * 	@return void
	**/
	public function edit_book($id){
		$this->load->library('form_validation');
		$data['book']= $this->db
			->where('id', $id)
			->get('books')->row_array();
		if (empty($data['book'])){
			show_404('page');
		}
		$rules = array(
			array(
				'field'   => 'name',
				'label'   => '"Название книги"',
				'rules'   => 'trim|required|min_length[3]'
			),
			array(
				'field'   => 'summary',
				'label'   => '"Аннотация"',
				'rules'   => 'trim|required|min_length[10]'
			),
		);
		$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() === TRUE){
			$post= $this->input->post(NULL, TRUE);
			$update= array(
				'name' => $post['name'],
				'thumb' => $post['thumb'],
				'summary' => $post['summary'],
			);
			if (!empty($post['authors'])){
				$this->db->where('book_id', $id)->delete('books_authors');
				$a_ins= array();
				foreach ($post['authors'] as $pa){
					$a_ins[]= array('book_id'=>$id, 'author_id'=>$pa);
				}
				$this->db->insert_batch('books_authors', $a_ins);
			}
			if (!empty($post['categories'])){
				$this->db->where('book_id', $id)->delete('books_categories');
				$c_ins= array();
				foreach ($post['categories'] as $pc){
					$c_ins[]= array('book_id'=>$id, 'category_id'=>$pc);
				}
				$this->db->insert_batch('books_categories', $c_ins);
			}

			$this->db->where('id', $id)->update('books', $update);
			redirect('/admin/books');
		}
		else{
			$data['title']= 'Редактировать книгу | '.$this->title;			
			$data['authors']= $this->admin_model->get_all_authors(array('fields'=>'id, name'));
			$data['categories']= $this->admin_model->get_categories();

			$book_cats= $this->admin_model->get_categories(array($data['book']['id']));			
			$data['book_cats']= array_keys($book_cats[$data['book']['id']]);

			$book_authors= $this->admin_model->get_authors(array($data['book']['id']));			
			$data['book_authors']= array_keys($book_authors[$data['book']['id']]);

			$this->load->view('admin/add_edit_book', $data);
		}
	}
	
	/**
	 * 	Страница добавления автора
	 * 	@return void
	**/
	public function add_author(){
		$this->load->library('form_validation');
		//проверка полей формы на адекватность
		$rules = array(
			array(
				'field'   => 'name',
				'label'   => '"ФИО автора"',
				'rules'   => 'trim|required|min_length[3]'
			),
			array(
				'field'   => 'description',
				'label'   => '"Биография"',
				'rules'   => 'trim|required|min_length[10]'
			),
		);
		$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() === TRUE){
			$post= $this->input->post(NULL, TRUE);
			$post['rating']= 0;
			
			$this->db->insert('authors', $post);
			$id= $this->db->insert_id();
			redirect('/admin/edit_author/'.$id);
		}
		else{
			$data['title']= 'Добавить автора | '.$this->title;
			$this->load->view('admin/add_edit_author', $data);
		}
	}
	
	/**
	 * 	Страница редактирования автора
	 * 	@param int $id			айди автора
	 * 	@return void
	**/
	public function edit_author($id){
		$this->load->library('form_validation');
		$data['author']= $this->db
			->where('id', $id)
			->get('authors')->row_array();
		if (empty($data['author'])){
			show_404('page');
		}
		//проверка полей формы на адекватность
		$rules = array(
			array(
				'field'   => 'name',
				'label'   => '"ФИО автора"',
				'rules'   => 'trim|required|min_length[3]'
			),
			array(
				'field'   => 'description',
				'label'   => '"Биография"',
				'rules'   => 'trim|required|min_length[10]'
			),
		);
		$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() === TRUE){
			$post= $this->input->post(NULL, TRUE);
			$post['rating']= $data['author']['rating']==NULL ? 0 : $data['author']['rating'];

			$this->db->where('id', $id)->update('authors', $post);
			redirect('/admin/authors/');
		}
		else{
			$data['title']= 'Редактировать автора | '.$this->title;
			$this->load->view('admin/add_edit_author', $data);
		}
	}
	
	/**
	 * 	Страница добавления жанра
	 * 	@return void
	**/
	public function add_category(){
		$this->load->library('form_validation');
		//проверка полей формы на адекватность
		$config = array(
		   array(
				 'field'   => 'name',
				 'label'   => '"Название"',
				 'rules'   => 'trim|required|min_length[3]'
			  ),
		   array(
				'field'   => 'url',
				'label'   => '"Url"',
				'rules'   => 'trim|required|min_length[3]'
			),
		);
		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() === TRUE){
			$post= $this->input->post(NULL, TRUE);
			$this->db->insert('categories', $post);
            $data['categories']= $this->admin_model->get_categories();
            redirect('/admin/categories/');
		} else {
			$data['title']= 'Добавить жанр | '.$this->title;
			$this->load->view('admin/add_edit_category', $data);
		}
	}
	
	/**
	 * 	Страница редактирования жанра
	 * 	@param int $id			айди жанра
	 * 	@return void
	**/
	public function edit_category($id){
		$this->load->library('form_validation');
		$data['category']= $this->db
			->where('id', $id)
			->get('categories')->row_array();
		if (empty($data['category'])){
			show_404('page');
		}

		//проверка полей формы на адекватность
		$config = array(
		   array(
				 'field'   => 'name',
				 'label'   => '"Название"',
				 'rules'   => 'trim|required|min_length[3]'
			  )
		);
		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() === TRUE){
			$post= $this->input->post(NULL, TRUE);
			$this->db->where('id', $id)->update('categories', $post);
			redirect('/admin/categories/');
		}
		else{
			$data['title']= 'Редактировать жанр | '.$this->title;
			$this->load->view('admin/add_edit_category', $data);
		}
	}

  
	public function form_page_url(){
		if (!$this->input->is_ajax_request()){
			exit('no direct access allowed');
		}
        $parent= '';
		$this->load->helper('translit_helper');
		$post=$this->input->post(NULL, TRUE);
        echo $parent._tr($post['name']);
	}	
}

