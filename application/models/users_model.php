<?php
/**
* 
*	Модель для работы с пользователями
*
**/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}
	
	public function encrypt_password($password, $username){
		return sha1(md5($password).md5($username));
	}
}
/* End Of File */
