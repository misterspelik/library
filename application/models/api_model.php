<?php
/**
* 
*	Модель для обработки запросов API
*
**/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model {
	public $log_file= 'api_log';
	
	function __construct(){
		parent::__construct();
		$this->load->library('log', array('name'=>$this->log_file));
	}
	
	/**
	 * 	Авторизация пользователя API
	 * 	@param array $params		данные пользователя
	 * 	@return array
	**/
	function authorize($params= array()){
		$this->load->model('users_model');
		$result = array('status'=>'OK', 'message' => '');

		$username = (isset($params['username'])) ? $params['username'] : '';
		$password = (isset($params['password'])) ? $params['password'] : '';
		
		$hash_password = $this->users_model->encrypt_password($password, $username);
		$query = $this->db
			->select('id')
			->where('login', $username)->where('password', $hash_password)
			->limit(1)->get('api_users');
		
		$log_data= array(
			'user'		=>	$username,
			'method'	=>	$params['method'],
			'ip'		=>	$_SERVER['REMOTE_ADDR']
		);
		$this->log->write_api($log_data);
		if ($query->num_rows == 0){
			$result= array(
				'status'	=> 'ERR',
				'message'	=> 'Incorrect authorization data for user: '.$username
			);
		}else{
			$uid= $query->row()->id;
			$result['privileges']= $this->get_user_privileges($uid);
		}
		return $result;
	}
	
	/**
	 * 	Получение привилегий пользователя
	 * 	@param int $user_id			айди пользователя API
	 * 	$return array				названия методов для доступа или 0=>'all'
	**/
	function get_user_privileges($user_id){
		$query= $this->db
			->select('privileges')
			->where('user_id', $user_id)->get('api_users_privileges');
		if ($query->num_rows == 0){
			return array();
		}
		$privileges= $query->row()->privileges;
		$result= explode(',', trim($privileges, ','));
		
		return $result;
	}

}
