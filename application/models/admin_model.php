<?php
/**
* 
*	Модель для работы в админке и по API
*
**/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}
	
	/**
	 * 	Возвращает все книги из таблицы
	 * 	@param int $limit		лимит
	 * 	@param int $offset		смещение
	 * 	@param arr $params		дополнительные параметры выборки
	 * 	@return array
	**/
	public function get_books($limit=0, $offset=0, $params=array()){
		if (!empty($params['fields'])){
			$this->db->select($params['fields']);
		}
		if(!empty($limit)){
			if (!empty($offset)){
				$this->db->limit($limit, $offset);
			}else{
				$this->db->limit($limit);
			}
		}
		if (!empty($params['order'])){
			$this->db->order_by($params['order'], $params['order_way']);
		}
		if (!empty($params['ids'])){
			$this->db->where_in('id', $params['ids']);
		}
		$res= $this->db->get('books')->result_array();
		$books= array();
		foreach($res as $r){
			$books[$r['id']]= $r;
		}
		return $books;
	}

	/**
	 * 	Возвращает всех авторов из таблицы
	 * 	@param array $params		параметры запроса
	 * 	@return array
	**/	
	public function get_all_authors($params=array()){
		if (empty($params)){
			return array();
		}
		if(!empty($params['limit'])){
			if (!empty($params['offset'])){
				$this->db->limit($params['limit'], $params['offset']);
			}else{
				$this->db->limit($params['limit']);
			}
		}
		if (!empty($params['fields'])){
			$this->db->select($params['fields']);
		}
		return $this->db
			->get('authors')->result_array();
	}
	
	public function get_all_clients(){
		return array();
	}
	
	/**
	 * 	Возвращает авторов книг
	 * 	(авторы рассортированы по книгам)
	 * 	@param array $books_ids			айдишники книг
	 * 	@return array
	**/
	public function get_authors($books_ids){
		if (empty($books_ids)){
			return array();
		}
		$res= $this->db->select('a.*, ba.book_id')
			->join('authors a', 'a.id=ba.author_id')
			->where_in('ba.book_id', $books_ids)
			->from('books_authors ba')
			->get()->result_array();
		$authors= array();
		foreach($res as $r){
			if(!isset($authors[$r['book_id']])){
				$authors[$r['book_id']]= array();
			}
			$authors[$r['book_id']][$r['id']]= $r;
		}
		return $authors;
		
	}

	/**
	 * 	Возвращает жанры книг
	 * 	(если указаны айди книг, то сортирует по книге её жанры)
	 * 	@param array $books_ids			айди книг
	 * 	@return array
	**/
	public function get_categories($books_ids= array()){
		$result= array();
		if (!empty($books_ids)){
			$res= $this->db->select('c.*, bc.book_id')
				->join('categories c', 'c.id=bc.category_id')
				->where_in('bc.book_id', $books_ids)
				->from('books_categories bc')
				->get()->result_array();
			foreach($res as $r){
				if(!isset($result[$r['book_id']])){
					$result[$r['book_id']]= array();
				}
				$result[$r['book_id']][$r['id']]= $r;
			}
		}else{
			$result= $this->db->get('categories')->result_array();
		}
		return $result;
	}

	public function get_book_by_id($id){
		return $this->db
            ->where('id', $id)
			->get('books')
            ->result_array();
	}
    
    public function get_books_by_category($category_id){
		return $this->db
            ->select('*')
            ->from('books')
            ->where('category_id',$category_id)
            ->join('books_categories', 'books_categories.book_id = books.id')
            ->get()
            ->result_array();
	}
    
    public function get_category_by_id($id){
		return $this->db
            ->where('id', $id)
			->get('categories')
            ->result_array();
	}
    
     public function get_category_by_url($url) {
        return $this->db
        ->select('*')
        ->from('categories')
        ->where('url', $url)
        ->get()
        ->result_array();
    }
     
	
	
}
/* End Of File */
