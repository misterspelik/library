<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Settings_model extends CI_Model {
		
		//конструктор модели
		function __construct(){
			parent::__construct();
		}
		
		//получение версии программы
		function getVersionName(){
			return $this->db->query("SELECT value FROM settings WHERE name='version'")->row()->value;
		}
				
		//возвращаем все настройки с ключами массива
		function getAllSettings(){
			$result= $this->db
					->select("name, label, value")
					->get("settings")
					->result_array();
					
			$sets= array();
			foreach ($result as $r){
				$sets[$r['name']]['value']= $r['value'];
				$sets[$r['name']]['label']= $r['label'];
			}
			return $sets;
		}
	}
?>
